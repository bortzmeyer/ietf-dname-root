# ietf-dname-root

Internet-Draft about putting DNAMEs in the DNS root for special-use TLDs like .local

For the [DNSOP](https://datatracker.ietf.org/wg/dnsop/)  working group of the [IETF](https://www.ietf.org/) : draft on DNAMEs records in the root zone, to redirect .local and friends to AS 112. Work started at the IETF 94 meeting in Yokohama (Japan) on 5 november 2015.

Author : Stéphane Bortzmeyer <bortzmeyer@nic.fr>

Reference site : https://framagit.org/bortzmeyer/ietf-dname-root
